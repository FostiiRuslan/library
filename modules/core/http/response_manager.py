from bottle import route, get, post, response


class Response(object):

    def __init__(self, classObj):
        self.classObj = classObj

    def __call__(self, *args, **kwargs):
        response = self.classObj(*args, **kwargs)
        return response()

@Response
class Http_response(object):

    def __init__(self, content='', mimetype="text/html", status=False):
        self.content = content
        response.status = 200
        response.content_type = str(mimetype)
        response.content_length = str(len(self.content))

        if status:
            response.status = status

    def __call__(self):

        return str(self.content)

    def __setitem__(self, key, value):
        response.__dict__[key] = value
