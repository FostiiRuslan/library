from abc import ABCMeta
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, mapper
from settings import DB_NAME, ENCODE, ECHO


Engine = create_engine(DB_NAME, encoding=ENCODE, echo=ECHO)

BaseModel = declarative_base()

session = sessionmaker(bind=Engine)

metadata = MetaData()

Session = session()

class Model(object):

    def __init__(self):
        Session.add(self)

    def save(self):
        Session.commit()

"""
class CreateMapper(type):

    def __init__(cls, classname, supers, attrdict):
        fields = [key for key in cls.__dict__ if not str(key).count('__') and not hasattr(cls.__dict__[key], '__call__')]
        param = []

        for item in list(cls.__dict__.values()):
            if not isinstance(item, str) and not hasattr(item, '__call__'):
                if isinstance(item, tuple) or isinstance(item, list):
                    param.append(item[0])
                elif isinstance(item, str) or isinstance(item, type):
                    param.append(item)

        param.insert(0, metadata)
        param.insert(0, cls.__name__)
        users_table = Table(*param)

        class Mapper(Model):

            def __init__(self, *args):
                print fields
                print args
                for field, value in fields, args:
                    setattr(self, field, value)
                super(Mapper, self).save()

        ob = Mapper('1', 'Ruslan', 'Ed Jones', 'edspassword')
        print ob.__dict__

        #create tables
        Mapper.__name__ = cls.__name__
        metadata.create_all(Engine)
        mapper(Mapper, users_table)
"""