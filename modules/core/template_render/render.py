from settings import template_location, STATIC_URL, MEDIA_URL
from modules.core.http.response_manager import Http_response


def render_to_response(template, data, request):
    template = template_location.get_template(template)
    data['STATIC_URL'] = STATIC_URL
    data['MEDIA_URL'] = MEDIA_URL
    print data
    data = template.render(data)
    return Http_response(data)