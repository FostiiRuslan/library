from modules.core.models.models_manager import Engine, BaseModel, Model, metadata
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import backref
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
from sqlalchemy.orm import mapper, relationship, backref


class User(BaseModel):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    login = Column(String)
    password = Column(String)

    def __init__(self, login, password):
        self.login = login
        self.password = password

    def __repr__(self):
        return "<User('%s', '%s')>" % (self.login, self.password)


association_table = Table('books_authors', BaseModel.metadata,
    Column('book_id', Integer, ForeignKey('book.id')),
    Column('author_id', Integer, ForeignKey('author.id'), nullable=True)
)


class Book(BaseModel):
    __tablename__ = 'book'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    author = relationship("Author",
                    secondary=association_table,
                    backref=backref("books", lazy="dynamic"))

    def __init__(self, name, author):
        self.name = name
        self.author = author

    def __repr__(self):
        return "<Book('%s', '%s')>" % (self.name, self.author)


class Author(BaseModel):
    __tablename__ = 'author'
    id = Column(Integer, primary_key=True)
    fullname = Column(String)

    def __init__(self, fullname):
        self.fullname = fullname

    def __repr__(self):
        return "<Author('%s')>" % (self.fullname)


#create tables
BaseModel.metadata.create_all(Engine)
