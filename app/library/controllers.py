import wtforms
import random
import beaker.session


from settings import template_location
from modules.core.template_render.render import render_to_response
from modules.core.http.response_manager import Http_response
from models import User, Author, Book
from bottle import (route,
                    get,
                    post,
                    request,
                    response,
                    static_file,
                    error,
                    redirect)
from wsgiserialize.json import jsonize
from settings import STATIC_PATH
from sqlalchemy import update, delete, insert, select, or_
from wsgiform import form
from forms import AuthForm, AuthorForm, BookForm
from modules.core.models.models_manager import Session
from settings import SECRET_KEY
from hashlib import md5


session = beaker.session.Session(request)


def get_client_ip(request):
    return request.environ.get('REMOTE_ADDR')


def _json_response(data):
    return Http_response(data, mimetype="application/json")


def authentication(request):
    if (session.get("login") and
        request.COOKIES.get("login") and
        session.get("login") == request.COOKIES.get("login")):
        return True
    return False


@route('/')
def index():
    #user = User("Ruslan", md5("pwd").hexdigest())
    #Session.add(user)
    #Session.commit()
    return render_to_response('index.html', {}, request)


@route('/auth', method=['GET', 'POST'])
def auth():
    form = AuthForm(request.POST, csrf_context=get_client_ip(request))
    if request.method == "POST":
        if form.validate():
            user = Session.query(User).filter(
                User.login == request.forms.get("login"),
                User.password == md5(request.forms.get("password")).hexdigest()
            ).first()
            if user:
                key = md5(SECRET_KEY).hexdigest()
                session["login"] = key
                response.COOKIES["login"] = key
                return redirect("/")
            else:
                return error401(error)
    return render_to_response("forms/auth_form.html", {"form": form}, request)


@route('/search', method=['GET'])
def search():
    result = None
    auth_exists = False
    if request.method == "GET":
        if request.COOKIES.get("login"):
            if authentication(request):
                auth_exists = True
            if len(request.GET.get("query", '')):
                books_list = Session.query(Book).join(Book.author).filter(
                    or_(Book.name.like("%" + request.GET.get("query") + "%"),
                        Author.fullname.like("%" + request.GET.get("query") + "%")
                    )
                ).all()
            else:
                books_list = Session.query(Book).all()
        else:
            return render_to_response("error401.html", {}, request)
    else:
        return redirect('/')
    return render_to_response("search_books_list.html",
                              {"books": books_list,
                               "auth_exists": auth_exists
                              }, request
    )


@route('/authors_list')
def authors_list():
    auth_exists = False
    if authentication(request):
        auth_exists = True
    authors_list = Session.query(Author).all()
    return render_to_response("authors_list.html",
                              {"authors": authors_list,
                               "auth_exists": auth_exists},
                              request)


@route('/books_list')
def books_list():
    auth_exists = False
    if authentication(request):
        auth_exists = True
    books_list = Session.query(Book).all()
    return render_to_response("books_list.html",
                              {"books": books_list,
                               "auth_exists": auth_exists},
                              request)


@route('/create_author', method=['GET', 'POST'])
def create_author():
    form = AuthorForm(request.POST, csrf_context=get_client_ip(request))
    success = False
    if authentication(request):
        if request.method == "POST":
            if form.validate():
                author = Author(
                    fullname=request.forms.get("fullname"),
                )
                Session.add(author)
                Session.commit()
                success = True
    else:
        return error401(request)
    return render_to_response("forms/author_form.html",
                               {"form": form,
                                "success": success,
                                "info": request.GET.get("info")
                               }, request)


@route('/create_book', method=['GET', 'POST'])
def create_book():
    form = BookForm(request.POST, csrf_context=get_client_ip(request))
    form.author.query = Session.query(Author).all()
    success = False
    if authentication(request):
        if request.method == "POST":
            if form.validate():
                book = Book(
                    name=request.forms.get("name"),
                    author=Session.query(Author).filter(Author.id.in_(
                        request.forms.getlist("author"))
                    ).all()
                )
                Session.add(book)
                Session.commit()
                success = True
    else:
        return error401(request)
    try:
        return render_to_response("forms/book_form.html",
                                  {"form": form,
                                   "success": success},
                                  request)
    except TypeError:
        return redirect("/create_author?info='Create first author before create new Book!'")


@route('/delete_author/<id_author>', method=['GET', 'POST'])
def delete_author(id_author):
    if authentication(request):
        del_author = Session.query(Author).filter(
            Author.id == int(id_author)
        ).delete()
        Session.commit()
        return redirect("/authors_list")
    else:
        return error401(request)


@route('/delete_book/<id_book>', method=['GET', 'POST'])
def delete_book(id_book):
    if authentication(request):
        del_book = Session.query(Book).filter(Book.id == int(id_book)).delete()
        Session.commit()
        return redirect("/books_list")
    else:
        return error401(request)


@route('/author_manager/<id_author>', method=['GET', 'POST'])
def author_manager(id_author=False):
    form = AuthorForm(request.POST, csrf_context=get_client_ip(request))
    success = False
    if authentication(request):
        if request.method == "POST":
            if form.validate():
                update_author = Session.query(Author).filter(
                    Author.id == int(request.forms.get("id"))
                ).first()
                update_author.fullname = request.forms.get("fullname")
                Session.add(update_author)
                Session.commit()
                success = True
        else:
            author = Session.query(Author).filter(Author.id == int(id_author)).first()
            form.fullname.data = author.fullname
            form.id.data = author.id
    else:
        return error401(request)
    return render_to_response("forms/author_form.html",
                              {"form": form,
                               "success": success}, request)


@route('/book_manager/<id_book>', method=['GET', 'POST'])
def book_manager(id_book=False):
    form = BookForm(request.POST, csrf_context=get_client_ip(request))
    book = Session.query(Book).filter(Book.id == id_book).first()
    all_authors = Session.query(Author).all()
    related_authors = []
    form.author.query = Session.query(Author).all()
    success = False

    for item in Session.query(Book.id).join(Book.author).filter(Author.id == Book.author).all():
        related_authors.append(item[0])

    if authentication(request):
        if request.method == "POST":
            if form.validate():
                update_book = Session.query(Book).filter(
                    Book.id == int(request.forms.get("id"))
                ).first()
                update_book.name = request.forms.get("name")
                update_book.author = Session.query(Author).filter(
                    Author.id.in_(request.forms.getlist("author"))
                ).all()
                Session.add(update_book)
                Session.commit()
                success = True
        else:
            book = Session.query(Book).filter(Book.id == int(id_book)).first()
            form.name.data = book.name
            form.id.data = book.id
    else:
        return error401(request)
    return render_to_response("forms/book_form.html",
                               {"book": book,
                                "all_authors": all_authors,
                                "related_authors": related_authors,
                                "form": form,
                                "success": success
                               }, request)


@route('/author_review/<id_author>')
def author_review(id_author):
    book = Session.query(Author).filter(Author.id == int(id_author)).first()
    return render_to_response("author_review.html", {"author": book}, request)


@route('/book_review/<id_book>')
def book_review(id_book):
    book = Session.query(Book).filter(Book.id == int(id_book)).first()
    return render_to_response("book_review.html", {"book": book}, request)


@route('/static/:path#.+#')
def get_static(path):
    return static_file(path, root=STATIC_PATH)


@error(404)
def error404(error):
    return 'Nothing here, sorry'


@error(401)
def error401(error):
    return 'Permission denied'
