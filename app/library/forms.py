from wtforms import Form, StringField, validators, IntegerField, HiddenField, PasswordField
from wtforms.ext.sqlalchemy.fields import QuerySelectMultipleField
from wtforms.ext.csrf import SecureForm
from hashlib import md5
from settings import SECRET_KEY


class IPSecureForm(SecureForm):
    """
    Generate a CSRF token based on the user's IP. I am probably not very
    secure, so don't use me.
    """

    def generate_csrf_token(self, csrf_context):
        # csrf_context is passed transparently from the form constructor,
        # in this case it's the IP address of the user
        token = md5(SECRET_KEY + csrf_context).hexdigest()
        return token

    def validate_csrf_token(self, field):
        if field.data != field.current_token:
            raise ValueError('Invalid CSRF')


class AuthForm(IPSecureForm):
    login = StringField('Login', validators=[validators.input_required()])
    password = PasswordField('Password', validators=[validators.input_required()])


class BookForm(IPSecureForm):
    id = HiddenField()
    name = StringField('Name', validators=[validators.input_required()])
    author = QuerySelectMultipleField("Author", get_label='fullname')


class AuthorForm(IPSecureForm):
    id = HiddenField()
    fullname = StringField('fullname', validators=[validators.input_required()])
