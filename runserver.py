import bottle
from wsgiref.simple_server import make_server
import sys
import settings
from beaker.middleware import SessionMiddleware
from settings import session_opts
from modules.core.routing import urls_manager
from app.library.controllers import *

if __name__ == "__main__":
    if not settings.PATH in sys.path:
        sys.path.append(settings.PATH)
    bottle.run(host='localhost', port=8080)

app = bottle.default_app()
wsgi_app = SessionMiddleware(app, session_opts)