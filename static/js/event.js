$(document).ready(function(){

    $("#menu li a").hover(function(){
        $(this).animate({"marginLeft": "10px"}, 100);
    });

    $("#menu li a").mouseleave(function(){
        $(this).animate({"marginLeft": "0px"}, 100);
    });

    $("#search_form input[type=submit]").click(function(e){
        e.preventDefault();
        $.get("/search", {query: $(this).siblings("input[type=text]").val()}, function(resp){
            $("#content").html(resp);
        });
    });

    $(".delete").live("click", function(e){
        e.preventDefault();
        var res = confirm("Do you realy want delete this object?");

        if (res)
            $.get($(this).attr("href"), {}, function(resp){location.reload();});
    });
});
