import os
import sys
from jinja2 import Template, Environment, PackageLoader


APP_NAME = "library"

#The string form of the URL is dialect+driver://user:password@host/dbname[?key=value..], where dialect is a database name such as mysql, oracle, postgresql, etc., and driver the name of a DBAPI, such as psycopg2, pyodbc, cx_oracle, etc. Alternatively, the URL can be an instance of URL.
DB_NAME = 'sqlite:///db.sql'

ENCODE = "utf-8"

SECRET_KEY = '1234567890'

ECHO = True

PATH = os.path.dirname(__file__)

STATIC_PATH = os.path.join(PATH, 'static/')

STATIC_URL = "/static/"

MEDIA_URL = "/media/"

MEDIA_PATH = os.path.join(PATH, 'media/')

TEMPLATES_DIR = os.path.join(PATH, 'templates/')

session_opts = {
    'session.type': 'file',
    'session.cookie_expires': True,
}

template_location = Environment(loader=PackageLoader('templates', '/'))
